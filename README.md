# Group Project Express App

## Overview
- This application is an express app which seeds data for transactions, users, accounts, and categories data which is then displayed on web pages. You can create, edit, delete, or view details about each data set.
- This web app also displays web pages for each member, which includes a link to a page about them.

## Team
- Sammy Fields -> Working on the User section
- Nandini Yadav -> Working on the Categories section
- Sushma Rani Reddy -> Working on the Transactions section
- Harsha Vardhan Reddy -> Working on the Accounts section

# This Web App Uses:

- Node.js platform
- Express web framework
- EJS templating engine
- MVC design pattern
- Mongoose MongoDB object modeling
- Lodash for JavaScript object iteration and manipulation
- jQuery library for DOM manipulation
- BootStrap Material Design framework for responsive design
- nedb In-memory database
- Winston logger


## Link to the Public Repository

- Bitbucket Repo <https://bitbucket.org/Sam-Fields/node-express-mvc-ejs-start/issues?status=new&status=open>

## Link to Hosted Web App on Heroku

- Heroku App <https://dry-hollows-28936.herokuapp.com/>

## Code Organization

- app.js - Starting point for the application. Defines the express server, requires routes and models. Loads everything and begins listening for events.
- controllers/ - logic for handling client requests
- data/ - seed data loaded each time the application starts
- models/ - schema descriptions for custom data types
- routes/ - route definitions for the API
- utils/ - utilities for logging and seeding data
- views/ - EJS - embedded JavaScript and HTML used to create dynamic pages