// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')

// require each data file

const transactions = require('../data/transactions.json')
const accounts = require('../data/accounts.json')
const users = require('../data/users.json')
const categories = require('../data/categories.json')
//const puppies = require('../data/puppies.json')

// inject the app to seed the data

module.exports = (app) => {
  LOG.info('START seeder.')
  const db = {}

  // Transactions don't depend on anything else...................

  db.transactions = new Datastore()
  db.transactions.loadDatabase()

  // insert the sample data into our data store
  db.transactions.insert(transactions)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.transactions = db.transactions.find(transactions)
  LOG.debug(`${app.locals.transactions.query.length} transactions seeded`)

  // accounts don't depend on anything else .....................

  db.accounts = new Datastore()
  db.accounts.loadDatabase()

  // insert the sample data into our data store
  db.accounts.insert(accounts)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.accounts = db.accounts.find(accounts)
  LOG.debug(`${app.locals.accounts.query.length} accounts seeded`)

  // users need a customer beforehand .................................

  db.users = new Datastore()
  db.users.loadDatabase()

  // insert the sample data into our data store
  db.users.insert(users)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.users = db.users.find(users)
  LOG.debug(`${app.locals.users.query.length} users seeded`)

  // Each category needs a product and an order beforehand ...................

  db.categories = new Datastore()
  db.categories.loadDatabase()

  // insert the sample data into our data store
  db.categories.insert(categories)

  // initialize app.locals (these objects will be available to our controllers)
  app.locals.categories = db.categories.find(categories)
  LOG.debug(`${app.locals.categories.query.length} categories seeded`)

  // extra...

  // db.puppies = new Datastore()
  // db.puppies.loadDatabase()

  // // insert the sample data into our data store
  // db.puppies.insert(puppies)

  // // initialize app.locals (these objects will be available to our controllers)
  // app.locals.puppies = db.puppies.find(puppies)
  // LOG.debug(`${app.locals.puppies.query.length} puppies seeded`)

  LOG.info('END Seeder. Sample data read and verified.')
}
