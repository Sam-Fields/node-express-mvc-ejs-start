/** 
*  User model
*  Describes the characteristics of each attribute of a User.
*
* @author Sammy Fields <s526635@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const User = new mongoose.Schema({
  _id: { type: Number, required: true },
  name: { type: String, required: true },
  email: { type: String, required: true },
  birthDate: { type: Date, required: true, default: Date.now() },
  accountNumber: { type: Number, required: true, unique: true },
  balance: { type: Number, required: true },
})

module.exports = mongoose.model('User', User)
