/** 
*  account model
*  Describes the characteristics of each attribute in a account resource.
*
* @author Harsha Vardhan Reddy <s534688@nwmissouri.edu>

*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const AccountSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  Account_type: { type: String, required: true, default:'Checking' },
  Account_no: { type: Number, required: true, unique: true },
  Account_name: { type: String, required: true }
})

module.exports = mongoose.model('Accounts', AccountSchema)
