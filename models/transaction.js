/** 
*  Transaction model
*  Describes the characteristics of each attribute in a Transactions resource.
*
* @author Sushma Reddy <s534741@nwmissouri.edu>
*
*/

// see <https://mongoosejs.com/> for more information
const mongoose = require('mongoose')

const TransactionsSchema = new mongoose.Schema({
  _id: { type: Number, required: true },
  transaction_type:{type: String, required: true},
  transaction_date:{type: String, required: true},
  transaction_amount: { type: Number, required: true }
})

module.exports = mongoose.model('Transactions', TransactionsSchema)
